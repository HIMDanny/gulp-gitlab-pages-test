import gulp from 'gulp';
import { html, scss, server } from './gulp/tasks/index.js';
import { path } from './gulp/config.js';

// watch files and run tasks when files are changed (saved). More watchers can be added
const watch = () => {
  gulp.watch(path.src.html, html);
  gulp.watch(path.src.styles, scss);
};

const mainTasks = gulp.parallel(html, scss);

export const build = mainTasks;

// when task is exported in gulpfile.js, it can be run by the gulp command (check package.json "dev" script)
export const dev = gulp.series(mainTasks, gulp.parallel(watch, server));
