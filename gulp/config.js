const srcDir = './src';
const distDir = './public';

export const path = {
  src: {
    root: srcDir,
    html: `${srcDir}/*.html`,
    styles: `${srcDir}/styles/**/*.scss`,
    scripts: `${srcDir}/scripts/**/*.js`,
    images: `${srcDir}/images/**/*`,
  },
  dist: {
    html: distDir,
    styles: `${distDir}/styles`,
    scripts: `${distDir}/scripts`,
    images: `${distDir}/images`,
  },
  srcDir,
  distDir,
};
