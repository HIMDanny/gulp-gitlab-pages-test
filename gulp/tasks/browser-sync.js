import BS from 'browser-sync';
import { path } from '../config.js';

export const browserSync = BS.create();

// start browser sync server
const server = () => {
  browserSync.init({
    server: {
      baseDir: path.distDir,
    },
  });
};

export default server;
