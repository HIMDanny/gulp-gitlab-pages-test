import gulp from 'gulp';
import { browserSync } from './browser-sync.js';
import { path } from '../config.js';

// move html files from src to dist and reload a server
const html = () =>
  gulp
    .src(path.src.html)
    .pipe(gulp.dest(path.dist.html))
    .pipe(browserSync.stream());

export default html;
