import { deleteAsync } from 'del';
import { path } from '../config.js';

const reset = () => {
  return deleteAsync(path.distDir);
};

export default reset;
