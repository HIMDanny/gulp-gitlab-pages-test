import gulp from 'gulp';
import gulpSass from 'gulp-sass';
import * as dartSass from 'sass';
import { browserSync } from './browser-sync.js';
import { path } from '../config.js';

const sass = gulpSass(dartSass);

// compile scss to css, move files to dist and reload a server
const scss = () =>
  gulp
    .src(path.src.styles)
    .pipe(sass())
    .pipe(gulp.dest(path.dist.styles))
    .pipe(browserSync.stream());

export default scss;
