## Як запустити?

1. `npm i`
2. `npm run dev`

## Посилання

- [Документація Gulp](https://gulpjs.com/docs/en/getting-started/javascript-and-gulpfiles)
- [Gulp for beginners](https://css-tricks.com/gulp-for-beginners/)
